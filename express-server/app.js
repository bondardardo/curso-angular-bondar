var express = require("express"), cors = require("cors");
var app = express();

app.use(express.json());
app.use(cors());

//Donde va a ser publicado el servicio.
app.listen(3000, () => console.log("Servidor Online en el puerto 3000"));

var marcas = ["Honda","Peugeot","Renault","Citroen","Hyundai","Toyota","Fiat","Volkswagen"];
//Urls publicadas
//req.query.q nos indica que de la consulta nos interesa la variable q de la consulta en la app '/marcas?q='
app.get("/marcas", (req, res, next) => res.json(marcas.filter((c)=> c.toLocaleLowerCase().indexOf(req.query.q.toString().toLocaleLowerCase()) > -1)));

var misVehiculos = [];
app.get("/my", (req, res, next) => res.json(misVehiculos));

app.post("/my", (req, res, next) => {
    console.log(req.body);
    //misDestinos = req.body;
    misVehiculos.push(req.body.nuevo);
    res.json(misVehiculos);
});

app.get("/api/translation", (req, res, next) => res.json([
    {lang: req.query.lang, key: 'HOLA', value: 'HOLA ' + req.query.lang}
  ]));