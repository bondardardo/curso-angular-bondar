import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReservasRoutingModule } from './reservas-routing.module';
import { ListaReservasComponent } from './lista-reservas/lista-reservas.component';
import { ReservasDetalleComponent } from './reservas-detalle/reservas-detalle.component';
import { ReservasApiClientService } from './reservas-api-client.service';

//Es un inyector por defecto
@NgModule({
  declarations: [ListaReservasComponent, ReservasDetalleComponent],
  imports: [
    CommonModule,
    //se crea automaticamente al agregar --routing en la creacion del modulo
    ReservasRoutingModule
  ],
  //Registrar el proveedor que es nuestro servicio inyectable, si ReservasApiclintService 
  //tiene alguna dependencia debe agregarse antes.
  providers: [ReservasApiClientService]
})
export class ReservasModule { }
