import { Injectable } from '@angular/core';

//Es genera como servicio inyectable y puede ser usado como inyectado 
//en otros modulos/componentes(usa Inyect en el contructor)

@Injectable({
  providedIn: 'root'
})
export class ReservasApiClientService {

  constructor() { }

  //Metodo Get All
  getAll() {
    return [{ id: 1, name: 'uno' }, {id: 2, name: 'dos' }];
  }
}
