import { Directive, OnInit, OnDestroy } from '@angular/core';

@Directive({
  selector: '[appEspiame]'
})

//esta clase implementa OnInit y OnDestroy (Son interfaces)
export class EspiameDirective implements OnInit, OnDestroy {
  static nextId = 0;
  //Comilla hacia la derecha es la interpolacion de cadenas de String
  log = (msg: string) => console.log(`Evento #${++EspiameDirective.nextId} ${msg}`);
  //metodo que se llama igual a la interface con ng por delante  OnInit
  ngOnInit() { this.log(`########******** onInit`); }
  //metodo que se llama igual a la interface con ng por delante  OnDestroy
  ngOnDestroy() { this.log(`########******** onDestroy`); }
}
