import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, Injectable, InjectionToken, NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { StoreModule as NgRxStoreModule, ActionReducerMap, Store } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule} from '@ngrx/store-devtools';
import { HttpClientModule, HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import Dexie from 'dexie';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { VehiculoComponent } from './components/vehiculo/vehiculo.component';
import { ListaVehiculosComponent } from './components/lista-vehiculos/lista-vehiculos.component';
import { VehiculoDetalleComponent } from './components/vehiculo-detalle/vehiculo-detalle.component';
//import { VehiculosApiClient } from './models/vehiculos-api-client.model';
import { FormVehiculoComponent } from './components/form-vehiculo/form-vehiculo.component';
import { InitMyDataAction, intializeVehiculoState, reducerVehiculo, VehiculoEffects, VehiculoState } from './models/vehiculo-estado.model';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { AuthService } from './services/auth.service';
import { VentasComponent } from './components/ventas/ventas/ventas.component';
import { VentasMainComponent } from './components/ventas/ventas-main/ventas-main.component';
import { VentasMasInfoComponent } from './components/ventas/ventas-mas-info/ventas-mas-info.component';
import { VentasDetalleComponent } from './components/ventas/ventas-detalle/ventas-detalle.component';
import { ReservasModule } from './reservas/reservas.module';
import { Vehiculo } from './models/vehiculo.model';
import { from, Observable } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EspiameDirective } from './espiame.directive';
import { TrackearClickDirective } from './trackear-click.directive';

//Inicio app config 
//agregamos la conexion al servidor web
export interface AppConfig {
  apiEndpoint: String;
}
const APP_CONFIG_VALUE: AppConfig = {
  //se puede tomar de archivo de configuracion
  apiEndpoint: 'http://localhost:3000'
};
export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');
//fin app config

//Inicio Ruteo
//Nuevas Rutas hijas de las rutas raices
export const childrenRoutesVuelos: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: VentasMainComponent },
  { path: 'mas-info', component: VentasMasInfoComponent },
  { path: ':id', component: VentasDetalleComponent },
];

const routes: Routes = [
    {path: '', redirectTo: 'home', pathMatch: 'full'},
    {path: 'home', component: ListaVehiculosComponent},
    {path: 'vehic_detalle/:id', component: VehiculoDetalleComponent},
    { path: 'login', component: LoginComponent },
    {
      path: 'protected',
      component: ProtectedComponent,
      canActivate: [ UsuarioLogueadoGuard ]
    },
    //se agrega este nuevo elemento a las Rutas para que la raiz conozca a las rutas hijas solo si 
    //se esta logueado por canActivate, luego los hijos.
    {
      path: 'ventas',
      component: VentasComponent,
      canActivate: [ UsuarioLogueadoGuard ],
      children: childrenRoutesVuelos
    }
];
//fin Ruteo

//Agregar para evitar el error. ModuleWithProvider
declare module "@angular/core" {
  interface ModuleWithProviders<T = any> {
      ngModule: Type<T>;
      providers?: Provider[];
  }
}

//redux init
export interface AppState {
  vehiculos: VehiculoState;
};

const reducers: ActionReducerMap<AppState> = {
  vehiculos: reducerVehiculo
};

let reducersInitialState = {
    vehiculos: intializeVehiculoState()
};
//fin redux init

// app init
export function init_app(appLoadService: AppLoadService): () => Promise<any>  {
  return () => appLoadService.intializeVehiculosState();
}

@Injectable()
class AppLoadService {
  constructor(private store: Store<AppState>, private http: HttpClient) { }
  async intializeVehiculosState(): Promise<any> {
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndpoint + '/my', { headers: headers });
    const response: any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitMyDataAction(response.body));
  }
}
// fin app init

// dexie db
//Clase de la traduccion para guardarla localmente en el Storage
export class Translation {
  constructor(public id: number, public lang: string, public key: string, public value: string) {}
}

@Injectable({
  providedIn: 'root'
})
export class MyDatabase extends Dexie {
  vehiculos: Dexie.Table<Vehiculo, number>;
  translations: Dexie.Table<Translation, number>;

  constructor () {
      super('MyDatabase');
      //Solo guarda destinos
      this.version(1).stores({
        vehiculos: '++id, marca, modelo, imagenurl',
      });
      //guarda destinos y traducciones. Para no perder los datos de la version (1)
      this.version(2).stores({
        destinos: '++id, nombre, imagenUrl',
        translations: '++id, lang, key, value'
      });
  }
}

export const db = new MyDatabase();
// fin dexie db

// i18n ini - Es un cargador personalizado de traducciones
class TranslationLoader implements TranslateLoader {
  constructor(private http: HttpClient) { }

  getTranslation(lang: string): Observable<any> {
    const promise = db.translations
                      .where('lang')
                      .equals(lang)
                      .toArray()
                      .then(results => {
                                        //si la traduccion no esta en la BD de dexie local, consulto a express
                                        if (results.length === 0) {
                                          //Devuelve una nueva promesa si esta en la api
                                          return this.http
                                            .get<Translation[]>(APP_CONFIG_VALUE.apiEndpoint + '/api/translation?lang=' + lang)
                                            .toPromise()
                                            .then(apiResults => {
                                              db.translations.bulkAdd(apiResults);
                                              return apiResults;
                                            });
                                        }
                                        //Si lo tengo en la bd local retorno el array con la traduccion.
                                        return results;
                                      }).then((traducciones) => {
                                        console.log('traducciones cargadas:');
                                        console.log(traducciones);
                                        return traducciones;
                                      }).then((traducciones) => {
                                        //lo convertimos a como lo necesita el nxtranslate
                                        return traducciones.map((t) => ({ [t.key]: t.value}));
                                      });
    /*
    return from(promise).pipe(
      map((traducciones) => traducciones.map((t) => { [t.key]: t.value}))
    );
    */
   return from(promise).pipe(flatMap((elems) => from(elems)));
  }
}

function HttpLoaderFactory(http: HttpClient) {
  return new TranslationLoader(http);
}
// fin i18n


@NgModule({
  declarations: [
    AppComponent,
    VehiculoComponent,
    ListaVehiculosComponent,
    VehiculoDetalleComponent,
    FormVehiculoComponent,
    LoginComponent,
    ProtectedComponent,
    VentasComponent,
    VentasMainComponent,
    VentasMasInfoComponent,
    VentasDetalleComponent,
    EspiameDirective,
    TrackearClickDirective
    
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    //Rutas Raices
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgRxStoreModule.forRoot(reducers, { initialState: reducersInitialState }),
    EffectsModule.forRoot([VehiculoEffects]),
    StoreDevtoolsModule.instrument(),
    ReservasModule,
    //Importar el modulo de traducciones
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: (HttpLoaderFactory),
          deps: [HttpClient]
      }
    }),
    NgxMapboxGLModule
  ],
  providers: [
    //Ejemplo 1 sacamos del appmodule vehiculosapiclient se usa solo en vehiculo-detalle y lista-vehiculos
    //VehiculosApiClient, 
    AuthService, UsuarioLogueadoGuard,
    //Agrego el proveedor de Dexie
    MyDatabase,
    //agregamos el provider para la inyeccion de dependencia del config
    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
    //Agregamos componentes del app init, el multi: true sieve para inicializar varior modulos a la vez.
    //hay que reperir el proveedor para cada inicializacion.
    AppLoadService,
    { provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true }

  ],
  bootstrap: [AppComponent],
  
})
export class AppModule { }
