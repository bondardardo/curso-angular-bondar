import {
    reducerVehiculo,
    VehiculoState,
    intializeVehiculoState,
    InitMyDataAction,
    NuevoVehiculoAction
  } from './vehiculo-estado.model';
  import { Vehiculo } from './vehiculo.model';
  
  declare module "@angular/core" {
    interface ModuleWithProviders<T = any> {
        ngModule: Type<T>;
        providers?: Provider[];
    }
  }
  
  describe('reducerVehiculo', () => {
    it('should reduce init data', () => {
      //Setup - arma objetos a testear
      const prevState: VehiculoState = intializeVehiculoState();
      const action: InitMyDataAction = new InitMyDataAction(['vehiculo 1', 'vehiculo 2']);
      //Action - hacer la interatuar los objetos, lo que estamos probando es el codigo productivo
      const newState: VehiculoState = reducerVehiculo(prevState, action);
      //assertions -Son las verificaciones
      expect(newState.items.length).toEqual(2);
      expect(newState.items[0].marca).toEqual('vehiculo 1');
      //Tear Down - Si hay base de datos borrar las inserciones en BD
    });
  
    it('should reduce new item added', () => {
      //Setup - arma objetos a testear
      const prevState: VehiculoState = intializeVehiculoState();
      const action: NuevoVehiculoAction = new NuevoVehiculoAction(new Vehiculo('Ferrari', 'F300','https:'));
      //Action - hacer la interatuar los objetos, lo que estamos probando es el codigo productivo
      const newState: VehiculoState = reducerVehiculo(prevState, action);
      //assertions -Son las verificaciones
      expect(newState.items.length).toEqual(1);
      expect(newState.items[0].marca).toEqual('Ferrari');
      //Tear Down - Si hay base de datos borrar las inserciones en BD
    });
  });