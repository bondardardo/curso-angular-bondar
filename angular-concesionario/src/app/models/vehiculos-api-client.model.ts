import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { forwardRef, Inject, Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { BehaviorSubject, Subject } from 'rxjs';
import { AppConfig, AppState, APP_CONFIG, db } from '../app.module';
import { ElegidoFavoritoAction, NuevoVehiculoAction } from './vehiculo-estado.model';
import { Vehiculo } from './vehiculo.model';

@Injectable()
export class VehiculosApiClient {
 vehiculos: Vehiculo[] = [];
	
	/*agregamos al constructor el Store
	constructor(private store: Store<AppState>) {
	}*/

	//Cambia el contructor
	constructor(
		private store: Store<AppState>,
		@Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
		private http: HttpClient
	  ) {
	
	}
	
	//Nuevo Add el servidor web
	add(v:Vehiculo){
		//Agregamos el dispatch al Store
		//this.store.dispatch(new NuevoVehiculoAction(v));

		const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
		const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: v.marca }, { headers: headers });
		this.http.request(req).subscribe((data: HttpResponse<{}>) => {
			if (data.status === 200) {
				this.store.dispatch(new NuevoVehiculoAction(v));
				//Luego que todo sale bien lo almacenamos en con Dexie.
				const myDb = db;
				myDb.vehiculos.add(v);
				console.log('todos los destinos de la db!');
				myDb.vehiculos.toArray().then(vehiculos => console.log(vehiculos))
			}
		});
	
	}

	//Nuevo elegir desde el Store
	elegir(v: Vehiculo){
		//Agregamos el dispatch al Store
		this.store.dispatch(new ElegidoFavoritoAction(v));
	}

	getById(id: String): Vehiculo{
		return this.vehiculos.filter(function(d){ return d.id.toString()=== id})[0];
	}

	getAll(): Vehiculo[]{
		return this.vehiculos;
	}

} 