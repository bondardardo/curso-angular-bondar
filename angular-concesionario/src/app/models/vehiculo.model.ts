import {v4 as uuid} from 'uuid';

export class Vehiculo {
	private selected: boolean;
	public caracteristicas: String [];
	id = uuid();
	
	constructor(public marca: string, public modelo: string, public imagenurl: string, public votes:number = 0){
		this.caracteristicas = ['Varios Colores','Seguro Incluido','Financiación 100%'];
	}

	isSelected (): boolean {
		return this.selected;
	}

	setSelected(band: boolean) {
		this.selected = band;
	}

	voteUp() {
		this.votes++;
	}

	voteDown() {
		this.votes--;
	}

	voteReset() {
		this.votes = 0;
	}

}

