import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Vehiculo } from './vehiculo.model';
import { HttpClientModule} from '@angular/common/http';

//ESTADO
export interface VehiculoState{
    items: Vehiculo[];
    loading: boolean;
    marcado: Vehiculo;
}

//No se puede inicializar en el aqui porque rudex es asincronico
export function intializeVehiculoState() {
	return {
	  items: [],
	  loading: false,
	  marcado: null
	};
  }

//ACCIONES
export enum VehiculoActionTypes {
  NUEVO_VEHICULO = '[Vehiculo] Nuevo',
  ELEGIDO_FAVORITO = '[Vehiculo] Favorito',
  VOTE_UP = '[Vehiculo] Vote Up',
  VOTE_DOWN = '[Vehiculo] Vote Down',
  VOTE_RESET = '[Vehiculo] Vote Reset',
  //nuevaAccion
  INIT_MY_DATA = '[Vehiculos] Iniializar mis datos'
}

export class NuevoVehiculoAction implements Action {
  type =VehiculoActionTypes.NUEVO_VEHICULO;
  constructor(public vehic: Vehiculo) {}
}

export class ElegidoFavoritoAction implements Action {
  type = VehiculoActionTypes.ELEGIDO_FAVORITO;
  constructor(public vehic: Vehiculo) {}
}

//las Clases de Votos
export class VoteUpAction implements Action {
	type = VehiculoActionTypes.VOTE_UP;
	constructor(public vehic: Vehiculo) {}
}

export class VoteDownAction implements Action {
	type = VehiculoActionTypes.VOTE_DOWN;
	constructor(public vehic: Vehiculo) {}
}

export class VoteResetAction implements Action {
	type = VehiculoActionTypes.VOTE_RESET;
	constructor(public vehic: Vehiculo) {}
}

//clase de la accion de inicializar
export class InitMyDataAction implements Action {
	type = VehiculoActionTypes.INIT_MY_DATA;
	constructor(public vehiculos: string[]) {}
  }

export type VehiculoActions = NuevoVehiculoAction | ElegidoFavoritoAction
  | VoteUpAction | VoteDownAction | VoteResetAction | InitMyDataAction;

//REDUCERS
export function reducerVehiculo(
	state: VehiculoState,
	action: VehiculoActions
) : VehiculoState {
	switch (action.type) {
		
		case VehiculoActionTypes.INIT_MY_DATA: {
			const destinos: string[] = (action as InitMyDataAction).vehiculos;
			return {
				...state,
				items: destinos.map((d) => new Vehiculo(d, '',''))
			  };
		}

		case VehiculoActionTypes.NUEVO_VEHICULO: {
		  return {
		  		...state,
		  		items: [...state.items, (action as NuevoVehiculoAction).vehic]
		  	};
		}
		case VehiculoActionTypes.ELEGIDO_FAVORITO: {
		    state.items.forEach(x => x.setSelected(false));
		    let fav:Vehiculo = (action as ElegidoFavoritoAction).vehic;
		    fav.setSelected(true);
		    return {
		    	...state,
		  		marcado: fav
		    };
		}

		//Devuelve el mismo estado ...state es retornar el estado clonado
		case VehiculoActionTypes.VOTE_UP: {
		    const v:Vehiculo = (action as VoteUpAction).vehic;
		    v.voteUp();
		    return {
		    	...state
		    };
		}
		case VehiculoActionTypes.VOTE_DOWN: {
		    const v:Vehiculo = (action as VoteDownAction).vehic;
		    v.voteDown();
		    return {
		    	...state
		    };
		}

		case VehiculoActionTypes.VOTE_RESET: {
		    const v:Vehiculo = (action as VoteResetAction).vehic;
		    v.voteReset();
		    return {
		    	...state
		    };
		}

	}
	return state;
}

//EFFECTS
@Injectable()
export class VehiculoEffects {
  @Effect()
  nuevoAgregado$: Observable<Action> = this.actions$.pipe(
    ofType(VehiculoActionTypes.NUEVO_VEHICULO),
  	map((action:NuevoVehiculoAction) => new ElegidoFavoritoAction(action.vehic))
  );

  constructor(private actions$: Actions) {}
}