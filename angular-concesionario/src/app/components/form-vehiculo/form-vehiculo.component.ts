import { Component, EventEmitter, forwardRef, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators , FormControl, ValidatorFn} from '@angular/forms';
import {Vehiculo} from './../../models/vehiculo.model';
import{fromEvent} from 'rxjs';
import{map, filter, debounceTime,distinctUntilChanged,switchMap} from 'rxjs/operators';
import{ajax, AjaxResponse} from 'rxjs/ajax';
import { APP_CONFIG, AppConfig} from 'src/app/app.module';

@Component({
  selector: 'app-form-vehiculo',
  templateUrl: './form-vehiculo.component.html',
  styleUrls: ['./form-vehiculo.component.css']
})
export class FormVehiculoComponent implements OnInit {
  @Output() onItemAdder: EventEmitter<Vehiculo>;
  fg: FormGroup;
  minTexto = 3;
  searchResults: string[];

  //constructor(fb: FormBuilder) { 
    //Reemplazamos el contructor y agregamos forwardRef para evitar la Referencia Circular entre Appmodule y este componente.
    constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) { 
    this.onItemAdder = new EventEmitter();
   
    this.fg = fb.group({
        marca: ['', Validators.compose([
          Validators.required,
          this.textoValidador,
          this.textoValidadorParametro(this.minTexto)
        ])],
        modelo: ['', Validators.required],
        url: ['', this.urlValidador]
    });

    'Para mostrar en el log cambios en los formularios'
    this.fg.valueChanges.subscribe((form: any) =>{
      console.log('Cambio en el formulario: ', form);
    });
  }

  ngOnInit() {
    //Detecta a medida que se tipea en la caja marca, busca las sugerencias en un archivo.json
    const eleMarca = <HTMLInputElement>document.getElementById('marca');
    /*FromEvent escucha sobre los eventos que se definen y aplica operaciones en orden y si cumple
    pasa a la siguiente hasta terminar, con javascripy haria callbacks y recarga la pagina que es 
    lo que no se quiere*/
    fromEvent(eleMarca, 'input')
      .pipe(
          map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
          filter(text => text.length > 2),
          debounceTime(200),
          distinctUntilChanged(),
          //apunta al apiEndpoint que es la ruta del servicio.
          switchMap((text: string) => ajax(this.config.apiEndpoint + '/marcas?q=' + text))
        ).subscribe(ajaxResponse => this.searchResults = ajaxResponse.response);
        
        /* Ejemplo anterior con datos.json Fijo
        switchMap(()=> ajax('assets/datos.json'))
      ).subscribe(ajaxResponse => {
        this.searchResults = ajaxResponse.response
        //filtramos el resultado
        .filter(function(x){
          return x.toLowerCase().includes(eleMarca.value.toLocaleLowerCase());
        });
      });*/
  
  }

  /* Lo que antes se hacia en Lista de Vehiculos al guardar*/
  guardar(marca:string,modelo:string, url:string):boolean {
    const v = new Vehiculo(marca,modelo,url);
    this.onItemAdder.emit(v);
  	//console.log(this.vehiculos);
    return false;
  }

  /* el validador personalizado, resultado es un Json ejemplo textoValido : true */
  textoValidador (control: FormControl): { [s: string]: boolean} {
    const l = control.value.toString().trim().length;
    if (l > 0 && l < 5) {
      return {'textoInvalido': true};
    }
    return null;
  }

  textoValidadorParametro (minTexto: number): ValidatorFn {
    return (control: FormControl) : { [s: string]: boolean} | null => {
      const l = control.value.toString().trim().length;
      if (l > 0 && l < minTexto) {
        return {'minLonTexto': true};
      }
      return null;
    }
  }

  /*
  /* el validador personalizado, resultado es un Json ejemplo textoValido : true */
  urlValidador (control: FormControl): { [s: string]: boolean} {
    const pos1 = control.value.toString().toLowerCase().trim();
    
    /*Controlo que empiece con http o https*/
    if (((pos1.substr(0,7))!=='http://') && ((pos1.substr(0,8))!=='https://')) {
      return {'urlInvalido': true};
    }
   
    return null;
  }
  
}
