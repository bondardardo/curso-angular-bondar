import { Component, Inject, InjectionToken, OnInit } from '@angular/core';
import { VehiculosApiClient } from './../../models/vehiculos-api-client.model';
import { Vehiculo } from './../../models/vehiculo.model';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.module';

//Se Borro todos los Ejemplos de Injeccion de dependencias.

@Component({
  selector: 'app-vehiculo-detalle',
  templateUrl: './vehiculo-detalle.component.html',
  styleUrls: ['./vehiculo-detalle.component.css'],
  providers: [VehiculosApiClient],
  styles: ['mgl-map {height: 75vh;width: 75vw;']
})
export class VehiculoDetalleComponent implements OnInit {
  vehiculo: Vehiculo;

  style = {
    sources: {
      world: {
        type: 'geojson',
        data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
      }
    },
    version: 8,
    layers: [{
      'id': 'countries',
      'type': 'fill',
      'source': 'world',
      'layout': {},
      'paint': {
        'fill-color': '#6F788A'
      }
    }]
  };

  //El contructor apunta al anterior apiclient
  constructor(private route: ActivatedRoute, private vehiculosApiClient:VehiculosApiClient) {}

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    console.log("VER "+ id);
    this.vehiculo = this.vehiculosApiClient.getById(id);
  }

}
