import { state } from '@angular/animations';
import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from './../../app.module';
import { ElegidoFavoritoAction, NuevoVehiculoAction } from './../../models/vehiculo-estado.model';
import {Vehiculo} from './../../models/vehiculo.model';
import {VehiculosApiClient} from './../../models/vehiculos-api-client.model';

@Component({
  selector: 'app-lista-vehiculos',
  templateUrl: './lista-vehiculos.component.html',
  styleUrls: ['./lista-vehiculos.component.css'],
  //Agrego la inyeccion de dependencias aca Tipo 1, la que sacamos de appmodule
  //resuelve como hacer un new sobre el objeto directamente. Es similalar a lo definido en el @ngmodule del appmodule
  providers: [ VehiculosApiClient ]
})
export class ListaVehiculosComponent implements OnInit {
  //vehiculos: Vehiculo[]; Ya que usamos el apiclient creado
  @Output() onItemAdded: EventEmitter<Vehiculo>;
  updates: string[];
  todos;

  /*constructor() { Ya que usamos el apiclient creado
  	this.vehiculos = []; 
  } */

  constructor(public vehiculosApiClient: VehiculosApiClient, private store: Store<AppState>) {
    this.onItemAdded = new EventEmitter();
    this.updates=[];
    this.store.select(state => state.vehiculos.marcado)
      .subscribe(v => {
        if (v != null) {
          this.updates.push('Se ha elegido a ' + v.marca + '-' + v.modelo)
        }
      });
    
    store.select(state => state.vehiculos.items).subscribe(items => this.todos = items);

    //inicializar el subscribe y como al comienzo es null y espera una cadena 
    //hay que agregar un control y actualizo la variable updates
    /*
    this.vehiculosApiClient.subscribeOnChange((v:Vehiculo) => {
        if (v != null) {
          this.updates.push('Se ha elegido a ' + v.marca + '-' + v.modelo)
        }
    });*/
  }

  ngOnInit(): void {
  }

  /* Nuevo Add con el evento de despacho de REDUX*/
  add(v: Vehiculo) {
  	this.vehiculosApiClient.add(v);
    this.onItemAdded.emit(v);
    //this.store.dispatch(new NuevoVehiculoAction(v));
  }
  
  /* Como era antes de Semana 2 Leccion 2a
  guardar(marca:string,modelo:string, url:string):boolean {
    let v = new Vehiculo(marca,modelo,url);
    //this.vehiculos.push(new Vehiculo(marca,modelo,url))
    this.vehiculosApiClient.add(v);
    this.onItemAdded.emit(v);
  	//console.log(this.vehiculos);
  	return false;
  }*/
  
  //Nuevo Elegido con el evento de despacho de REDUX
  elegido(v: Vehiculo) {
    this.vehiculosApiClient.elegir(v);
    //this.store.dispatch(new ElegidoFavoritoAction(v));
  }

  getAll(){

  }

}
