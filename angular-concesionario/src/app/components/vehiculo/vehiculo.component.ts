import { Component, OnInit , Input, HostBinding, EventEmitter, Output } from '@angular/core';
import {Vehiculo} from './../../models/vehiculo.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { VoteDownAction, VoteUpAction, VoteResetAction } from './../../models/vehiculo-estado.model';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-vehiculo',
  templateUrl: './vehiculo.component.html',
  styleUrls: ['./vehiculo.component.css'],
  animations: [
    trigger('esFavorito', [
      state('estadoFavorito', style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNoFavorito', style({
        backgroundColor: 'WhiteSmoke'
      })),
      transition('estadoNoFavorito => estadoFavorito', [
        animate('3s')
      ]),
      transition('estadoFavorito => estadoNoFavorito', [
        animate('1s')
      ]),
    ]
    )]
})

export class VehiculoComponent implements OnInit {
  @Input() vehic: Vehiculo;
  @Input() linea: Number;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() clicked: EventEmitter<Vehiculo>
  
  constructor(private store: Store<AppState>) {
    this.clicked = new EventEmitter();
   }

  ngOnInit(): void {
  }

  ir (){
    this.clicked.emit(this.vehic);
    return false;
  }

  voteUp() {
    this.store.dispatch(new VoteUpAction(this.vehic));
    return false;
  }

  voteDown() {
    this.store.dispatch(new VoteDownAction(this.vehic));
    return false;
    
  }

  voteReset() {
    this.store.dispatch(new VoteResetAction(this.vehic));
    return false;
    
  }
}
