import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-concesionario';
  time = new Observable(observer => {
    //se define cada cuanto se llama
    //se puede usar para hacer ejecutar sql o actualizar posiciones de un mapa
    setInterval(() => observer.next(new Date().toString()), 1000); 
  });

  constructor(public translate: TranslateService) {
    console.log('***************** get translation');
    translate.getTranslation('en').subscribe(x => console.log('x: ' + JSON.stringify(x)));
    translate.setDefaultLang('es');
  }

}
